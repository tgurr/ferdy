# Copyright 2008 Fernando J. Pereda
# Distributed under the terms of the GPLv2
# Based in part upon 'open-vm-tools' ebuild from Gentoo, which is:
#     Copyright 1999-2007 Gentoo Foundation

require multibuild autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.10 ] ]

SUMMARY="Open VMWare Tools"
HOMEPAGE="http://open-vm-tools.sourceforge.net"

MY_PV=${PV:0:4}.${PV:4:2}.${PV:6:2}-142982

DOWNLOADS="mirror://sourceforge/${PN}/${PN}-${MY_PV}.tar.gz"

LICENCES="LGPL-2"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="X
    xinerama [[ requires = X ]]
    unity [[ requires = xinerama ]]"

DEPENDENCIES="
    X? (
        x11-libs/libXrandr
        x11-libs/libXtst
        x11-libs/gtk+
        xinerama? ( x11-libs/libXinerama )
        unity? (
            x11-libs/libXScrnSaver
            x11-libs/libXinerama
            sys-libs/zlib
            media-libs/libpng
            dev-libs/uriparser
        )
    )
    sys-process/procps
    dev-libs/libdnet
"

WORK="${WORKBASE}/${PN}-${MY_PV}"

src_prepare()
{
    default
    sed -i -e '/eval "$$DEPMOD"/d' modules/Makefile.am \
        || die 'sed modules/Makefile.am failed'
    sed -i -e "s-/lib/modules/-/${LIBDIR}/modules/-" configure.ac \
        || die 'sed configure.ac failed'
    eautoreconf
}

DEFAULT_SRC_CONFIGURE_PARAMS=( --without-icu --with-procps --with-dnet )
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( "xinerama multimon" unity )
DEFAULT_SRC_CONFIGURE_OPTION_WITHS=( "X x" )

src_compile()
{
    # This is needed because the kernel's ARCH variable is not the same as ours
    ARCH_save=${ARCH}
    unset ARCH

    emake RPCGEN="/usr/bin/rpcgen -Y /usr/bin"

    ARCH=${ARCH_save}
}

src_install()
{
    default

    if option X ; then
        dobin vmware-user-suid-wrapper/vmware-user-suid-wrapper
        edo chmod u+s "${IMAGE}"/usr/bin/vmware-user-suid-wrapper
    fi

    dodir /sbin
    mv "${IMAGE}"/usr/sbin/mount.vmhgfs "${IMAGE}"/sbin/mount.vmhgfs
}

